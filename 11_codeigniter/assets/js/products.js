$(document).ready(function () {

	$('.edit_product').click(function () {
		var categoryid = $(this).data('categoryid');
		var id = $(this).data('id');
		location.href = "/dashboard/editProduct/"+categoryid+"/" + id;
	});

	$('.add_product').click(function () {
		var categoryid = $(this).data('categoryid');
		location.href = "/dashboard/addProduct/"+categoryid;
	});


	$('.delete_product').click(function () {
		var categoryid = $(this).data('categoryid');
		var id = $(this).data('id');
		Swal.fire({
			title: "Delete this product ?",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete",
			cancelButtonText: "Cancel"
		}).then((result) => {
			if (result.value) {
				location.href = "/dashboard/deleteProduct/"+categoryid+"/"  + id;
			}
		})
	});



	$('.ascend, .descend').on('click', function () {
		var categoryid = $(this).data('categoryid');
		var id = $(this).data('id');
		var idToGo = $(this).data('idtogo');

		location.href = "/dashboard/modifyProductOrderPosition/"+categoryid+"/"  + id + "/" + idToGo;
	});


});

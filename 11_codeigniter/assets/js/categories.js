$(document).ready(function () {

	$('.edit_category').click(function () {
		var id = $(this).data('id');
		location.href = "/dashboard/editCategory/" + id;
	});

	$('.add_category').click(function () {
		location.href = "/dashboard/addCategory/";
	});


	$('.delete_category').click(function () {
		var id = $(this).data('id');
		Swal.fire({
			title: "Delete this category ?",
			text: "Your category has to be empty to be deleted.",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete",
			cancelButtonText: "Cancel"
		}).then((result) => {
			if (result.value) {
				location.href = "/dashboard/deleteCategory/" + id;
			}
		})
	});



	$('.ascend, .descend').on('click', function () {
		var id = $(this).data('id');
		var idToGo = $(this).data('idtogo');

		location.href = "/dashboard/modifyOrderPosition/" + id + "/" + idToGo;
	});

	$('.category_image').on('click', function () {
		$('.category_image').removeClass('selected');
		$(this).addClass('selected');
		$('#category_image').val($(this).data('file'));
	});

});

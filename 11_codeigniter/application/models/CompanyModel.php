<?php


class CompanyModel extends CI_Model
{
	function insert($data)
	{
		$this->db->insert('company', $data);
		return $this->db->insert_id();
	}

	function update($id, $data){
		$this->db->where('id', $id);
		$this->db->update('company', $data);
		return true;
	}

	function getCompanyById($id){
		$this->db->where('id', $id);
		$query = $this->db->get('company');
		return $query->row();
	}

	function getCompanies($userId)
	{
		$companies = array();

		$this->db->where('user_id', $userId);
		$query = $this->db->get('company');
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$companies[] = $row;
			}
		} else {
			$id = $this->createCompanie($userId);
			if ($id > 0) {
				$this->db->where('user_id', $userId);
				$query = $this->db->get('company');
				$companies[] = $query->result();
			}
		}

		return $companies;
	}

	function getAllCompanies()
	{
		$companies = array();
		$query = $this->db->get('company');
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$companies[] = $row;
			}
		} else {
			$id = $this->createCompanie($userId);
			if ($id > 0) {
				$this->db->where('user_id', $userId);
				$query = $this->db->get('company');
				$companies[] = $query->result();
			}
		}
		return $companies;
	}



	function createCompanie($userId)
	{
		$directory = random_string('alnum', 16);
		mkdir('assets/companies/' . $directory);
		$data = array('user_id' => $userId, 'directory' => $directory, 'name' => 'My awesome restaurant', 'menu_link' => 'smartmenu-'.$userId);
		return $this->insert($data);
	}

}

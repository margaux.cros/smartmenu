<?php


class ProductModel extends CI_Model
{
	function insert($data)
	{
		$this->db->insert('product', $data);
		return $this->db->insert_id();
	}

	function update($categoryId, $id, $data)
	{
		$this->db->where('id', $id);
		$this->db->where('category_id', $categoryId);
		$this->db->update('product', $data);
		return true;
	}

	function delete($categoryId, $id)
	{
		$this->db->where('id', $id);
		$this->db->where('category_id', $categoryId);
		$this->db->delete('product');
		return true;

		return false;
	}

	function getProducts($categoryId)
	{
		$products = array();

		$this->db->where('category_id', $categoryId);
		$this->db->order_by('order_position', 'ASC');
		$query = $this->db->get('product');
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$products[] = $row;
			}
		}

		return $products;
	}

	function getMaxOrderPosition($categoryId)
	{

		$this->db->select_max('order_position', 'max_order_position');
		$this->db->where('category_id', $categoryId);
		$query = $this->db->get('product');
		foreach ($query->result() as $row) {
			return $row->max_order_position;
		}

		return 0;
	}

	function getProductfromId($id){
		$this->db->where('id', $id);
		$query = $this->db->get('product');
		return $query->row();
	}

	function modifyOrderPosition($categoryId, $id, $idToGo)
	{
		$this->db->where('id', $idToGo);
		$query = $this->db->get('product');
		$positionToGo = intval($query->row()->order_position, 10);
		$categoryId = intval($query->row()->category_id, 10);

		$this->db->where('id', $id);
		$this->db->where('category_id', $categoryId);
		$this->db->update('product', array('order_position' => $positionToGo));

		$this->db->query('UPDATE product SET order_position = order_position + 1 WHERE ' . $positionToGo . '<=order_position AND category_id="' . $categoryId . '" AND id != "' . $id . '"');

	}


}

<?php


class CategoryModel extends CI_Model
{
	function insert($data)
	{
		$this->db->insert('category', $data);
		return $this->db->insert_id();
	}

	function update($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('category', $data);
		return true;
	}

	function delete($id)
	{
		//verif
		$this->db->where('category_id', $id);
		$query = $this->db->get('product');
		if ($query->num_rows() == 0) {
			$this->db->where('id', $id);
			$this->db->delete('category');
			return true;
		}

		return false;
	}

	function getCategories($companyId)
	{
		$categories = array();

		$this->db->where('company_id', $companyId);
		$this->db->order_by('order_position', 'ASC');
		$query = $this->db->get('category');
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$categories[] = $row;
			}
		}

		return $categories;
	}

	function getMaxOrderPosition($companyId)
	{

		$this->db->select_max('order_position', 'max_order_position');
		$this->db->where('company_id', $companyId);
		$query = $this->db->get('category');
		foreach ($query->result() as $row) {
			return $row->max_order_position;
		}

		return 0;
	}

	function modifyOrderPosition($id, $idToGo)
	{
		$this->db->where('id', $idToGo);
		$query = $this->db->get('category');
		$positionToGo = intval($query->row()->order_position, 10);
		$companyId = intval($query->row()->company_id, 10);

		$this->db->where('id', $id);
		$this->db->update('category', array('order_position' => $positionToGo));

		//die('UPDATE category SET order_position = order_position + 1 WHERE ' . $positionToGo . '<=order_position AND company_id="' . $companyId . '" AND id != "' . $id . '"');
		$this->db->query('UPDATE category SET order_position = order_position + 1 WHERE ' . $positionToGo . '<=order_position AND company_id="' . $companyId . '" AND id != "' . $id . '"');


	}


}

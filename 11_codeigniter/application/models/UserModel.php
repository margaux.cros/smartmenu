<?php


class UserModel extends CI_Model
{
	function insert($data)
	{
		$this->db->insert('user', $data);
		return $this->db->insert_id();
	}

	function verifyEmail($key)
	{
		$this->db->where('verification_key', $key);
		$this->db->where('is_mail_verified', false);
		$query = $this->db->get('user');

		if ($query->num_rows() > 0) {
			$data = array(
				'is_mail_verified' => true
			);
			$this->db->where('verification_key', $key);
			$this->db->update('user', $data);
			
			return true;
		}

		return false;
	}

	function canLogin($email, $password)
	{
		$this->db->where('email', $email);
		$query = $this->db->get('user');
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				if($row->is_mail_verified){
					if($password == $row->password){
						$this->session->set_userdata('id', $row->id);
						return true;
					}else{
						return 'Wrong Password';
					}
				}else{
					return 'First verified your email address';
				}

			}

		}

		return 'Wrong Email Adress';
	}
}

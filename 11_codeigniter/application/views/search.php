<div class="container">
	<div class="page-header">
		<h1 class="align-center">Welcome !</h1>
		<p class="lead">Find the best address</p>
	</div>
	<div class="row">
		<?php foreach ($companies as $company) { ?>
			<div class="card m-3 shadow" style="width: 18rem;">
				<div style="height:14rem">
				<img class="card-img-logo" src="<?= base_url() ?><?= 'assets/companies/' . $company->directory . '/logo.png' ?>" alt="logo">
				</div>
				<div class="card-body ">
					<h5 class="card-title"><?= $company->name ?></h5>
					<p class="card-text"></p>
					<a href="menu/company/<?=$company->id ?>" class="btn btn-red">Voir le menu</a>
				</div>
			</div>
		<?php } ?>
	</div>


</div>



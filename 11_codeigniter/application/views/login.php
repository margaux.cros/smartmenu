<div class="container">
	<div class="page-header">
		<h1 class="align-center">Connexion</h1>
		<p class="lead">Login and start using SmartMenu</p>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

			<?php if ($this->session->flashdata('message')) { ?>
				<div class="p-3 alert alert-danger">
					<?= $this->session->flashdata('message') ?>
				</div>
			<?php } ?>

			<span class="text-danger"></span>
			<form class="p-3" method="post" action="<?= base_url(); ?>login/validation">
				<div class="form-group">
					<label> Enter Email Adress </label>
					<input type="email" name="user_email" class="form-control" value="<?= set_value('user_email'); ?>"/>
					<span class="text-danger"><?= form_error('user_email'); ?></span>
				</div>
				<div class="form-group">
					<label> Enter Password </label>
					<input type="password" name="user_password" class="form-control"
						   value="<?= set_value('user_password'); ?>"/>
					<span class="text-danger"><?= form_error('user_password'); ?></span>
				</div>
				<div class="form-group">
					<input class="btn btn-primary" type="submit" name="login" value="Login">
				</div>
			</form>
		</div>
	</div>


	<div class="alert alert-light"> Don't have an account ? <a href="<?= base_url(); ?>register">Register</a></div>

</div>



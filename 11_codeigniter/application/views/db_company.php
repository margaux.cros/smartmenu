<div class="shadow p-2 mb-3 bg-white rounded"><h2>Company</h2></div>

<div class="shadow p-3 mb-3 bg-white rounded">
	<form class="p-3" method="post" action="<?= base_url(); ?>dashboard/company">
		<div class="p-3 mb-2 bg-info text-light">This form contains company informations such as address, website...
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group pb-4">
					<label> Company name </label>
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="fas fa-cash-register"></i></div>
						</div>
						<input type="text" name="company_name" class="form-control"
							   value="<?= set_value('company_name') == false ? $company->name : set_value('company_name'); ?>"/>
						<div class="text-danger w-100"><?= form_error('company_name'); ?></div>
					</div>
				</div>


				<div class="row">
				<div class="col-3 form-group">
					<label> Street Number </label>
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="fas fa-sort-numeric-up-alt"></i></div>
						</div>
						<input type="text" name="company_street_number" class="form-control"
							   value="<?= set_value('company_street_number') == false ? $company->street_number: set_value('company_street_number'); ?>"/>
						<div class="text-danger w-100"><?= form_error('company_street_number'); ?></div>
					</div>
				</div>
				<div class="col-9 form-group">
					<label> Route </label>
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="fas fa-road"></i></div>
						</div>
						<input type="text" name="company_route" class="form-control"
							   value="<?= set_value('company_route') == false ? $company->route: set_value('company_route'); ?>"/>
						<div class="text-danger w-100"><?= form_error('company_route'); ?></div>
					</div>
				</div>
				</div>
				<div class="row">
				<div class="col-3 form-group">
					<label> Postal code </label>
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="fas fa-map-marked-alt"></i></div>
						</div>
						<input type="text" name="company_postal_code" class="form-control"
							   value="<?= set_value('company_postal_code') == false ? $company->postal_code: set_value('company_postal_code'); ?>"/>
						<div class="text-danger w-100"><?= form_error('company_postal_code'); ?></div>
					</div>
				</div>
				<div class="col-9 form-group">
					<label> Locality </label>
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="fas fa-envelope"></i></div>
						</div>
						<input type="text" name="company_locality" class="form-control"
							   value="<?= set_value('company_locality') == false ? $company->locality: set_value('company_locality'); ?>"/>
						<div class="text-danger w-100"><?= form_error('company_locality'); ?></div>
					</div>
				</div>
				</div>
				<div class="form-group">
					<label> Country </label>
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="fas fa-flag"></i></div>
						</div>
						<input type="text" name="company_country" class="form-control"
							   value="<?= set_value('company_country') == false ? $company->country: set_value('company_country'); ?>"/>
						<div class="text-danger w-100"><?= form_error('company_country'); ?></div>
					</div>
				</div>


			</div>

			<div class="col-sm-6">
				<div class="form-group">
					<label> Menu link </label>
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="mr-1 fas fa-mobile-alt"></i>smartmenu/</div>
						</div>
						<input type="text" name="company_menu_link" class="form-control"
							   value="<?= set_value('company_menu_link') == false ? $company->menu_link: set_value('company_menu_link'); ?>"/>
						<div class="text-danger w-100"><?= form_error('company_menu_link'); ?></div>
					</div>
				</div>
				<div class="form-group">
					<label> Website </label>
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="fas fa-globe"></i></div>
						</div>
						<input type="text" name="company_website" class="form-control"
							   value="<?= set_value('company_website') == false ? $company->website: set_value('company_website'); ?>"/>
						<div class="text-danger w-100"><?= form_error('company_website'); ?></div>
					</div>
				</div>
				<div class="form-group">
					<label> Phone number </label>
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="fas fa-phone-alt"></i></div>
						</div>
						<input type="text" name="company_phone" class="form-control"
							   value="<?= set_value('company_phone') == false ? $company->phone: set_value('company_phone'); ?>"/>
						<div class="text-danger w-100"><?= form_error('company_phone'); ?></div>
					</div>
				</div>

			</div>
		</div>

		<?php if ($this->session->flashdata('message')) { ?>
			<div class="p-3 alert alert-success">
				<?= $this->session->flashdata('message') ?>
			</div>
		<?php } ?>


		<div class="form-group">
			<input class="btn btn-primary" type="submit" name="update" value="Update">
		</div>
	</form>
</div>

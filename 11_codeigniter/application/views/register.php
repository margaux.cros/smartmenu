<div class="container">
	<div class="page-header">
		<h1 class="align-center">Registration</h1>
		<p class="lead">
			Complete User Registration to start using Smartmenu
		</p>
	</div>


	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

			<?php if ($this->session->flashdata('message')) { ?>
				<div class="p-3 alert alert-info">
					<?= $this->session->flashdata('message') ?>
				</div>
			<?php } ?>

			<span class="text-danger"></span>
			<form class="p-3" method="post" action="<?= base_url(); ?>register/validation">
				<div class="form-group">
					<label> Email </label>
					<input type="email" name="user_email" class="form-control" value="<?= set_value('user_email'); ?>"/>
					<span class="text-danger"><?= form_error('user_email'); ?></span>
				</div>
				<div class="form-group">
					<label> Name </label>
					<input type="text" name="user_name" class="form-control" value="<?= set_value('user_name'); ?>"/>
					<span class="text-danger"><?= form_error('user_name'); ?></span>
				</div>
				<div class="form-group">
					<label> Password </label>
					<input type="password" name="user_password" class="form-control"
						   value="<?= set_value('user_password'); ?>"/>
					<span class="text-danger"><?= form_error('user_password'); ?></span>
				</div>
				<div class="form-group">
					<input class="btn btn-primary" type="submit" name="register" value="Register">
				</div>
			</form>
			<div class="alert alert-light">Already have an account ? <a href="<?= base_url(); ?>login">Log in</a></div>
		</div>
	</div>
</div>


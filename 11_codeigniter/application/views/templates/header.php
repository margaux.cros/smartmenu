<header id="header">
	<nav class="navbar navbar-expand-lg py-3 sticky-top navbar-dark bg-dark shadow-sm">
		<div class="container">
			<a href="/" class="navbar-brand">
				<!-- Logo Image -->
				<img class="navbar-brand-logo" src="<?=base_url()?>assets/images/smartmenu">
				<!-- Logo Text -->
				<span class="text-uppercase font-weight-bold">Smartmenu</span>
			</a>

			<button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>

			<div id="navbarSupportedContent" class="collapse navbar-collapse">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active"><a href="/"" class="nav-link">Home <span class="sr-only">(current)</span></a></li>
					<li class="nav-item"><a href="<?=base_url()?>login" class="nav-link">Login</a></li>
					<li class="nav-item"><a href="<?=base_url()?>register" class="nav-link">Register</a></li>
					<li class="nav-item"><a href="<?=base_url()?>menu" class="nav-link">Search</a></li>
				</ul>
			</div>
		</div>
	</nav>
</header>

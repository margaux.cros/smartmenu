<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Smartmenu</title>

	<!-- styles -->

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/styles.css">
</head>
<body>

<?php if ($header) {
	echo $header;
} ?>


<?php if ($page) {
	echo $page;
} ?>


<?php if ($footer) {
	echo $footer;
} ?>




<script src="<?= base_url() ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url() ?>assets/js/popper.js"></script>
<script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/main.js"></script>


</body>
</html>

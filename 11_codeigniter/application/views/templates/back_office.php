<!doctype html>
<html lang="fr">
<head>
	<title>Back Office - Smartmenu</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">

	<!-- styles -->

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/styles.css">
</head>
<body>

<div class="wrapper d-flex align-items-stretch">
	<nav class="bg-dark" id="sidebar">
		<div class="custom-menu">
			<button type="button" id="sidebarCollapse" class="btn btn-primary bg-dark">
				<i class="fa fa-bars"></i>
				<span class="sr-only">Toggle Menu</span>
			</button>
		</div>
		<div class="p-4">

			<h1><a href="<?= base_url() ?>dashboard" class="logo"><span class="text-uppercase">Smartmenu </span><span
						class="mbr-author-desc mbr-fonts-style display-7">A modern restaurant menu</span></a></h1>
			<ul class="list-unstyled components mb-5">
				<li class="menu-item">
					<a href="<?= base_url() ?>dashboard"><i class="icon fas fa-tachometer-alt mr-3"></i>Dashboard</a>
				</li>
				<li class="menu-item">
					<a href="<?= base_url() ?>dashboard/company"><i class="icon fas fa-cash-register mr-3"></i> Company</a>
				</li>
				<li class="menu-item">
					<a href="<?= base_url() ?>dashboard/customization"><i class="icon fas fa-brush mr-3"></i> Customization</a>
				</li>
				<li class="menu-item">
					<a href="<?= base_url() ?>dashboard/categories"><i class="icon fas fa-book mr-3"></i> Categories</a>
				</li>
				<li class="menu-item">
					<i class="icon fas fa-pizza-slice mr-3"></i> Products
					<ul>
						<?php foreach ($categories as $category) {?>
							<li>
								<a href="<?= base_url() ?>dashboard/products/<?= $category->id?>"><?= $category->name?></a>
							</li>
						<?php } ?>
					</ul>
				</li>
				<li class="menu-item">
					<a href="<?= base_url() ?>home/logout"><i class="icon fas fa-power-off mr-3"></i> Log out</a>
				</li>


			</ul>

		</div>
	</nav>

	<!-- Page Content  -->
	<div id="db-content" class="bg-light container-fluid p-4 p-md-5 pt-5">
		<?php if ($page) {
			echo $page;
		} ?>
	</div>
</div>


<script src="<?= base_url() ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url() ?>assets/js/popper.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/main.js"></script>
<script src="<?= base_url() ?>assets/js/categories.js"></script>
<script src="<?= base_url() ?>assets/js/products.js"></script>
</body>
</html>

<div class="shadow p-2 mb-3 bg-white rounded"><h2>Dashboard</h2></div>

<div class="row">
	<div class="col-md-4">
		<div class="shadow p-3 mb-3 bg-white rounded">
			<div class="db-logo">
				<img class="img-fluid m-auto d-block" src="<?= base_url() ?><?= $company->logo ?>"/>
			</div>
			<div class="row pl-1  pt-2">
				<div class="col-sm-1 text-red">
					<i class="fas fa-map-marker-alt"></i>
				</div>
				<div class="col-sm-11 p-3">
					<?= $company->street_number ?> <?= $company->route ?> <br/>
					<?= $company->postal_code ?> <br/>
					<?= $company->locality ?> <br/>
					<?= $company->country ?> <br/>
				</div>
			</div>
			<div class="row pl-1 pt-2">
				<div class="col-sm-1 text-red">
					<i class="fas fa-phone-alt"></i>
				</div>
				<div class="col-sm-11 p-3">
					<?= $company->phone ?> <br/>
				</div>
			</div>
			<div class="row pl-1 pt-2">
				<div class="col-sm-1 text-red">
					<i class="fas fa-globe"></i>
				</div>
				<div class="col-sm-11 p-3">
					<?= $company->website ?> <br/>
				</div>
			</div>
			<div class="row pl-1 pt-2"><a href="<?= base_url() ?>dashboard/company">Update contact
					informations</a></div>
		</div>


	</div>
	<div class="col-md-8">
		<div class="shadow p-3 pl-md-5 mb-3 bg-white rounded">
			<div class="row pl-1 pb-4">
				<div class="col-12 p-3"
				">
				<h3><i class=" mr-2  fas fa-cash-register"></i><?= $company->name ?></h3>
				<p><a href="<?= base_url() ?>dashboard/company">Change company name</a></p>
			</div>
		</div>
		<div class="row pl-1 pb-4">
			<div class="col-12 p-3">
				<h3><i class=" mr-2 fas fa-mobile-alt"></i>Restaurant menu link</h3>
				<p>You can access to your menu using <a href="<?= base_url() ?>smartmenu/<?= $company->menu_link ?>">smartmenu/<?= $company->menu_link ?></a>
				</p>
				<p><a href="<?= base_url() ?>dashboard/company">Change Menu link</a></p>
			</div>
		</div>
		<div class="row pl-1 pb-4">
			<div class="col-12 p-3">
				<h3><i class="mr-2 fas fa-book"></i></i>Categories</h3>
				<p>Your menu contains <?= count($categories) ?> categories </p>
				<p><a href="<?= base_url() ?>dashboard/categories">Manage categories</a></p>
			</div>
		</div>
		<div class="row pl-1 pb-4">
			<div class="col-12 p-3">
				<h3><i class="mr-2 fas fa-pizza-slice"></i></i>Products</h3>
				<p>Your menu contains products</p>
			</div>
		</div>
	</div>
</div>

</div>


<div class="shadow p-2 mb-3 bg-white rounded"><h2><?=$currentCategory->name ?> Products</h2></div>



<div class="p-3 row justify-content-end">
	<button data-categoryid ="<?=$currentCategory->id ?>" class="btn btn-success add_product"><i class="mr-2 fas fa-plus"></i> Add Product</button>
</div>
<div class="shadow mb-3 bg-white rounded">
	<table class="table table-hover">
		<thead>
		<tr>
			<th scope="col"></th>
			<th scope="col">Order</th>
			<th scope="col">Name</th>
			<th scope="col"></th>
			<th scope="col"></th>
		</tr>
		</thead>
		<tbody>


		<?php
		$i = 0;
		while ($i < sizeof($products)) {
			$product = $products[$i] ?>
			<tr>
				<td>
					<?php if ($product->image) { ?>
						<img style="max-width:80px" class="img-fluid"
							 src=" <?= base_url(); ?>assets/companies/<?= $company->directory?>/products/<?= $product->image?>">
					<?php } ?>
				</td>
				<td>
					<?php if ($i < sizeof($products)-1) { ?>
						<button class="btn descend"data-categoryid ="<?=$currentCategory->id ?>" data-id="<?= $products[$i+1]->id ?>" data-idToGO="<?=$product->id ?> "><i
								class="fas fa-long-arrow-alt-down"></i></button>
					<?php } ?>
					<?php if ($i > 0) { ?>
						<button class="btn ascend"data-categoryid ="<?=$currentCategory->id ?>"  data-id="<?= $product->id ?>" data-idToGO="<?=$products[$i-1]->id ?>" ><i
								class="fas fa-long-arrow-alt-up"></i>
						</button>
					<?php } ?>

				</td>
				<td><?= $product->name ?></td>
				<td>
					<button data-categoryid ="<?=$currentCategory->id ?>" data-id="<?= $product->id ?>" class="btn btn-warning edit_product">Edit Product
					</button>
				</td>
				<td>
					<button data-categoryid ="<?=$currentCategory->id ?>" data-id="<?= $product->id ?>" class="btn btn-danger delete_product">Delete Product
					</button>
				</td>
			</tr>
			<?php $i++;
		} ?>


		</tbody>
	</table>
</div>


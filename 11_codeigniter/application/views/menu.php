<div class="container">
	<div class="row">
	<div class="col-md-5">
		<div>
			<div>
				<img class="img-fluid" <img src="<?= base_url() ?>assets/companies/<?= $company->directory ?>/logo.png">
			</div>
			<div>
				<h1><?=$company->name?></h1>
			</div>
			<div>
				<?=$company->street_number?> <?=$company->route ?> <br/>
				<?=$company->postal_code?> <?=$company->locality ?>, <?=$company->country ?>
			</div>
		</div>
	</div>
	<div class="col-md-7">
		<?php
		foreach ($menu as $category) { ?>
			<div class="ribbon">
				<h3><?= $category->name ?><img src="<?= base_url() ?>assets/images/icones/<?= $category->image ?>"></h3>
				<ul class="list-unstyled">
					<?php foreach ($category->products as $product) { ?>
						<li>
							<div class="row">
								<div class="col-9">
									<?= $product->name ?>
								</div>

								<div class="col-3 text-right">
									<?= $product->price ?> €
								</div>
							</div>


							<?php if ($product->image != null) { ?>
								<img class="img-fluid d-block m-auto"
									 src="<?= base_url() ?>assets/companies/<?= $company->directory ?>/products/<?= $product->image ?>">
							<?php } ?>
						</li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
	</div>
	</div>
</div>

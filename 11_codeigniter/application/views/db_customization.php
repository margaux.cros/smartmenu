<div class="shadow p-2 mb-3 bg-white rounded"><h2>Customization</h2></div>


<div class="shadow p-3 mb-3 bg-white rounded">
	<form class="p-3" method="post" enctype="multipart/form-data" action="<?= base_url(); ?>dashboard/customization/upload">
		<div class="p-3 mb-2 bg-info text-light">Build your online menu to fit your graphic charter.</div>

		<div class=" mb-4 row">
			<div class="col-5">
				<div>Your logo</div>
				<div class="db-logo">
					<img class="img-fluid m-auto d-block" src="<?= base_url() ?><?= $company->logo ?>"/>
				</div>
			</div>
			<div class="col-7">
				<div class="form-group">
					<label for="exampleFormControlFile1">Upload your logo</label>
					<input type="file" class="form-control-file" name="company_logo">
					<div class="mt-4 form-group">
						<input class="btn btn-primary" type="submit" name="upload" value="Upload">
					</div>
				</div>
			</div>

		</div>

		<?php if ($this->session->flashdata('message')) { ?>
			<div class="p-3 alert alert-success">
				<?= $this->session->flashdata('message') ?>
			</div>
		<?php } ?>
	</form>
</div>

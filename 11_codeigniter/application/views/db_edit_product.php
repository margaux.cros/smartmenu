<div class="shadow p-2 mb-3 bg-white rounded"><h2>Edit Product</h2></div>


<div class="shadow p-3 mb-3 bg-white rounded">
	<form class="p-3" method="post" enctype="multipart/form-data"
		  action="<?= base_url(); ?>dashboard/editProduct/<?= $currentCategory->id; ?>/<?= $editedProduct->id; ?>/save_edit">
		<div class="p-3 mb-2 bg-info text-light">Update my product</div>



		<div class=" mb-4 row">
			<div class="col-4">
				<div class=" form-group">
					<label> Name </label>
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="fas fa-pizza-slice"></i></div>
						</div>
						<input type="text" name="product_name" class="form-control"
							   value="<?= set_value('product_name') == false ? $editedProduct->name : set_value('product_name'); ?>"/>
						<div class="text-danger w-100"><?= form_error('product_name'); ?></div>
					</div>
				</div>
			</div>

			<div class="col-8">
				<div class="form-group">
					<label> Description </label>
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="fas fa-info-circle"></i></div>
						</div>
						<input type="text" name="product_description" class="form-control"
							   value="<?= set_value('product_description') == false ? $editedProduct->name : set_value('product_description'); ?>"/>
						<div class="text-danger w-100"><?= form_error('product_description'); ?></div>
					</div>
				</div>
			</div>
		</div>



		<div class="row mb-4">
			<div class="col-4 form-group">
				<label> Price </label>
				<div class="input-group">
					<div class="input-group-prepend">
						<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
					</div>
					<input type="number" name="product_price" class="form-control"
						   value="<?= set_value('product_description') == false ? $editedProduct->description : set_value('product_description'); ?>"/>
					<div class="text-danger w-100"><?= form_error('product_price'); ?></div>
				</div>
			</div>
		</div>

		<div class="row mb-4">
			<div class="col-sm-4 form-group">
				<label> Image </label>
				<input type="file" class="form-control-file" name="product_image">

			</div>
			<div class="col-sm-8">
				<?php if ($editedProduct->image) { ?>
					<img class=" img-fluid
			"
						 src=" <?= base_url(); ?>assets/companies/<?= $company->directory ?>/products/<?= $editedProduct->image ?>">
				<?php } ?>
			</div>
		</div>


		<div class="form-group">
			<input class="btn btn-primary" type="submit" name="edit" value="Edit">
		</div>
	</form>
</div>





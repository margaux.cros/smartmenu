<div class="shadow p-2 mb-3 bg-white rounded"><h2>Categories</h2></div>


<div class="p-3 row justify-content-end">
	<button class="btn btn-success add_category"><i class="mr-2 fas fa-plus"></i> Add Category</button>
</div>

<?php if ($this->session->flashdata('message')) { ?>
	<div class="p-3 alert alert-success">
		<?= $this->session->flashdata('message') ?>
	</div>
<?php } ?>
<div class="shadow mb-3 bg-white rounded">
	<table class="table table-hover">
		<thead>
		<tr>
			<th scope="col"></th>
			<th scope="col">Order</th>
			<th scope="col">Product category</th>
			<th scope="col"></th>
			<th scope="col"></th>
		</tr>
		</thead>
		<tbody>


		<?php
		$i = 0;
		while ($i < sizeof($categories)) {
			$category = $categories[$i] ?>
			<tr>
				<td>
					<?php if ($category->image) { ?>
						<img style="max-width:80px" class="img-fluid"
							 src=" <?= base_url(); ?>assets/images/icones/<?= $category->image ?>">
					<?php } ?>
				</td>
				<td>
					<?php if ($i < sizeof($categories)-1) { ?>
						<button class="btn descend" data-id="<?= $categories[$i+1]->id ?>" data-idToGO="<?=$category->id ?> "><i
								class="fas fa-long-arrow-alt-down"></i></button>
					<?php } ?>
					<?php if ($i > 0) { ?>
						<button class="btn ascend" data-id="<?= $category->id ?>" data-idToGO="<?=$categories[$i-1]->id ?>" ><i
								class="fas fa-long-arrow-alt-up"></i>
						</button>
					<?php } ?>

				</td>
				<td><?= $category->name ?></td>
				<td>
					<button data-id="<?= $category->id ?>" class="btn btn-warning edit_category">Edit Category
					</button>
				</td>
				<td>
					<button data-id="<?= $category->id ?>" class="btn btn-danger delete_category">Delete Category
					</button>
				</td>
			</tr>
			<?php $i++;
		} ?>


		</tbody>
	</table>
</div>





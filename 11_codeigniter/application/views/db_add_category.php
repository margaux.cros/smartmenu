<div class="shadow p-2 mb-3 bg-white rounded"><h2>Add Category</h2></div>


<div class="shadow p-3 mb-3 bg-white rounded">
	<form class="p-3" method="post" action="<?= base_url(); ?>dashboard/addCategory/add_new">
		<div class="p-3 mb-2 bg-info text-light">Create a category to organize products</div>

		<div class=" mb-4 row">
			<div class="col-4">
				<div class=" form-group">
					<label> Name </label>
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="fas fa-book"></i></i></i></div>
						</div>
						<input type="text" name="category_name" class="form-control"
							   value="<?= set_value('category_name'); ?>"/>
						<div class="text-danger w-100"><?= form_error('category_name'); ?></div>
					</div>
				</div>
			</div>
			<div class="col-8">
				<div class="form-group">
					<label> Description </label>
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text"><i class="fas fa-info-circle"></i></i></i></div>
						</div>
						<input type="text" name="category_description" class="form-control"
							   value="<?= set_value('category_description'); ?>"/>
						<div class="text-danger w-100"><?= form_error('category_description'); ?></div>
					</div>
				</div>
			</div>

			<div class=" mb-4">
				<div class="col-12 form-group">
					<label> Category image </label>
					<div class="input-group row">
						<?php foreach (scandir('assets/images/icones') as $file) {
							if (strlen($file) > 3) {
								?>
								<img class="category_image" src=" <?= base_url(); ?>assets/images/icones/<?= $file ?>"
									 data-file="<?= $file ?>" style="display: inline;">
							<?php }
						} ?>
					</div>
				</div>

				<input type="hidden" name="category_image" id="category_image" value="0">
			</div>

		</div>

		<div class="form-group">
			<input class="btn btn-primary" type="submit" name="create" value="Create">
		</div>
	</form>
</div>






<div class="container">
	<div class="media-container-row row">
		<div class="media-content col-lg-7 px-3 align-self-center mbr-white py-2">
			<h1 class="mbr-text testimonial-text mbr-fonts-style display-7"><span style="font-style: normal;"></span></h1>
			<p class="mbr-author-name pt-4 mb-2 mbr-fonts-style display-1">
				Smartmenu.
			</p>
			<p class="mbr-author-desc mbr-fonts-style display-7">
				A modern restaurant menu
			</p>
		</div>

		<div class="mbr-figure p-3 col-lg-5 ">
			<img class="img-fluid shadow-sm" src="<?=base_url()?>assets/images/app_illustration.png" title="" media-simple="true">
		</div>
	</div>
</div>

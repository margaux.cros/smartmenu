<?php


class Menu extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
//		$this->load->library('encrypt');
		$this->load->helper('url');
		$this->load->model('companymodel');
		$this->load->model('productmodel');
		$this->load->model('categorymodel');

		$this->data['companies'] = $this->companymodel->getAllCompanies();
	}


	public function index()
	{
		$this->page = 'search';
		$this->layout();
	}

	public function company($id)
	{
		$this->data['company'] = $this->companymodel->getCompanyById($id);

		if(isset($this->data['company']->id) and $this->data['company']->id>0){
			$this->data['menu'] = $this->getMenu($id);

			$this->page = 'menu';
			$this->layout();
		}else{
			redirect('menu');
		}


	}

	private function getMenu($id){
		$categories = $this->categorymodel->getCategories($id);
		$menu = array();

		foreach ($categories as $category){
			$menu[$category->id] = $category;
			$menu[$category->id]->products = $this->productmodel->getProducts($category->id);
		}

		return $menu;
	}

}

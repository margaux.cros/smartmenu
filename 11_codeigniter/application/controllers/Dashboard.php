<?php


class Dashboard extends MY_Controller
{
	private $company = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');

		$this->load->model('companymodel');
		$this->load->model('categorymodel');
		$this->load->model('productmodel');

		$this->load->helper('string');
		$this->load->helper('url');

		if (!$this->session->has_userdata('id')) {
			redirect('home');
		}

		$companies = $this->companymodel->getCompanies($this->session->id);
		$this->data['company'] = end($companies);
		$this->data['company']->logo = $this->getLogo();

		$this->data['categories'] = $this->categorymodel->getCategories($this->data['company']->id);
	}

	public function index()
	{
		$this->page = 'dashboard';
		$this->backOfficeLayout();
	}

	public function company()
	{
		if ($this->input->post('update')) {
			$this->form_validation->set_rules('company_name', 'Name', 'required|trim');
			$this->form_validation->set_rules('company_street_number', 'Street Number', 'trim');
			$this->form_validation->set_rules('company_route', 'Route', 'trim');
			$this->form_validation->set_rules('company_locality', 'Locality', 'trim');
			$this->form_validation->set_rules('company_postal_code', 'Postal code', 'trim');
			$this->form_validation->set_rules('company_country', 'Country', 'trim');

			$this->form_validation->set_rules('company_menu_link', 'Menu Link', 'trim');
			$this->form_validation->set_rules('company_website', 'Website', 'trim|valid_url');
			$this->form_validation->set_rules('company_phone', 'Phone', 'trim');
			if ($this->form_validation->run()) {
				$companyData = array(
					'name' => $this->input->post('company_name'),
					'street_number' => $this->input->post('company_street_number'),
					'route' => $this->input->post('company_route'),
					'locality' => $this->input->post('company_locality'),
					'postal_code' => $this->input->post('company_postal_code'),
					'country' => $this->input->post('company_country'),
					'menu_link' => $this->input->post('company_menu_link'),
					'website' => $this->input->post('company_website'),
					'phone' => $this->input->post('company_phone'),
				);

				if ($this->companymodel->update($this->data['company']->id, $companyData)) {
					$this->session->set_flashdata('message', 'Informations have been successfully registered ');
				}

			}
		}

		$this->page = 'db_company';
		$this->backOfficeLayout();
	}

	public function customization()
	{
		if ($this->input->post('upload')) {
			$this->do_upload();
		}
		$this->page = 'db_customization';
		$this->backOfficeLayout();
	}

	public function categories()
	{
		if ($this->input->post('upload')) {
			$this->do_upload();
		}
		$this->page = 'db_categories';
		$this->backOfficeLayout();
	}


	public function products($category_id)
	{

		$currentCategory = $this->getCategory($category_id);

		if ($currentCategory != false) {

			$this->data['currentCategory'] = $currentCategory;
			$this->data['products'] = $this->productmodel->getProducts($category_id);

			$this->page = 'db_products';
			$this->backOfficeLayout();
		} else {
			redirect('dashboard/categories');
		}


	}

	public function addCategory($add_new = false)
	{
		if (!$add_new) {
			$this->page = 'db_add_category';
			$this->backOfficeLayout();
		} else {
			$this->form_validation->set_rules('category_name', 'Name', 'required|trim');

			if ($this->form_validation->run()) {
				$maxOrderPosition = $this->categorymodel->getMaxOrderPosition($this->data['company']->id);
				$categoryData = array(
					'name' => $this->input->post('category_name'),
					'description' => $this->input->post('category_description'),
					'image' => $this->input->post('category_image'),
					'company_id' => $this->data['company']->id,
					'order_position' => intval($maxOrderPosition) + 1
				);

				if ($this->categorymodel->insert($categoryData)) {
					redirect('dashboard/categories');
				}

			} else {
				$this->addCategory();
			}
		}
	}

	public function editCategory($id, $save_edit = false)
	{
		if ($id) {
			if (!$save_edit) {
				$this->page = 'db_edit_category';
				$edited_category = $this->getCategory($id);
				if ($edited_category != false) {
					$this->data['edited_category'] = $edited_category;
					$this->backOfficeLayout();
				} else {
					redirect('dashboard/categories');
				}

			} else {
				$this->form_validation->set_rules('category_name', 'Name', 'required|trim');

				if ($this->form_validation->run()) {
					$categoryData = array(
						'name' => $this->input->post('category_name'),
						'description' => $this->input->post('category_description'),
						'image' => $this->input->post('category_image'),
					);

					if ($this->categorymodel->update($id, $categoryData)) {
						redirect('dashboard/categories');
					}
				}
			}
		} else {
			redirect('dashboard/categories');
		}

	}

	private function getCategory($categoryId)
	{
		$currentCategory = null;
		foreach ($this->data['categories'] as $category) {
			if ($category->id == $categoryId) {
				$currentCategory = $category;
			}
		}
		if ($currentCategory != null) {
			return $currentCategory;
		}

		return false;
	}


	public function deleteCategory($id)
	{
		if ($id > 0) {
			if (!$this->categorymodel->delete($id)) {
				$this->session->set_flashdata('message', 'Delete Category failed');
			}

		}
		redirect('dashboard/categories');
	}

	public function modifyOrderPosition($id, $idToGo)
	{
		if ($id > 0) {
			$this->categorymodel->modifyOrderPosition($id, $idToGo);
		}
		redirect('dashboard/categories');
	}

	public function addProduct($categoryId, $add_new = false)
	{

		$currentCategory = $this->getCategory($categoryId);

		if ($currentCategory != false) {
			if (!$add_new) {
				$this->data['currentCategory'] = $currentCategory;
				$this->page = 'db_add_product';
				$this->backOfficeLayout();
			} else {
				$this->form_validation->set_rules('product_name', 'Name', 'required|trim');

				$product_image = $this->uploadProducts();


				if ($this->form_validation->run()) {
					$maxOrderPosition = $this->productmodel->getMaxOrderPosition($this->data['company']->id);
					$categoryData = array(
						'name' => $this->input->post('product_name'),
						'description' => $this->input->post('product_description'),
						'price' => $this->input->post('product_price'),
						'image' => $product_image,
						'category_id' => $categoryId,
						'order_position' => intval($maxOrderPosition) + 1
					);

					if ($this->productmodel->insert($categoryData)) {
						redirect('dashboard/products/' . $categoryId);
					}

				} else {
					$this->addProduct($categoryId);
				}
			}
		} else {
			redirect('dashboard/categories');
		}
	}

	public function modifyProductOrderPosition($categoryId, $id, $idToGo)
	{
		$currentCategory = $this->getCategory($categoryId);

		if ($currentCategory != false) {

			if ($id > 0) {
				$this->productmodel->modifyOrderPosition($categoryId, $id, $idToGo);
			}

		}
		redirect('dashboard/products/' . $categoryId);
	}


	public function deleteProduct($categoryId, $id)
	{
		if ($id > 0) {
			$currentCategory = $this->getCategory($categoryId);

			if ($currentCategory != false) {
				if (!$this->productmodel->delete($categoryId, $id)) {
					$this->session->set_flashdata('message', 'Delete Product failed');
				}
			}

		}
		redirect('dashboard/products/' . $categoryId);
	}

	public function editProduct($categoryId, $id, $save_edit = false)
	{
		$currentCategory = $this->getCategory($categoryId);

		if ($currentCategory != false) {
			if ($id) {
				if (!$save_edit) {
					$this->page = 'db_edit_product';
					$this->data['currentCategory'] = $currentCategory;
					$this->data['editedProduct'] = $this->productmodel->getProductfromId($id);
					$this->backOfficeLayout();

				} else {
					$this->form_validation->set_rules('product_name', 'Name', 'required|trim');
					$product_image = $this->uploadProducts();
					if ($this->form_validation->run()) {
						$productData = array(
							'name' => $this->input->post('product_name'),
							'description' => $this->input->post('product_description'),
							'price' => $this->input->post('product_price'),
						);

						if ($product_image != '') {
							$productData['image'] = $product_image;
						}

						if ($this->productmodel->update($categoryId, $id, $productData)) {
							redirect('dashboard/products/' . $categoryId);
						}
					} else {
						$this->editProduct($categoryId, $id);
					}
				}
			} else {
				redirect('dashboard/products/' . $categoryId);
			}
		} else {
			redirect('dashboard/categories');
		}


	}


	public function getLogo()
	{
		if (!file_exists('assets/companies/' . $this->data['company']->directory . '/logo.png')) {
			return 'assets/images/no-logo-available.png';
		}

		return 'assets/companies/' . $this->data['company']->directory . '/logo.png';
	}

	public function do_upload()
	{
		$config['upload_path'] = 'assets/companies/' . $this->data['company']->directory . '/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name'] = 'logo.png';
		$config['overwrite'] = TRUE;
		$config['max_size'] = 6000;
		$config['max_width'] = 2048;
		$config['max_height'] = 2048;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('company_logo')) {
			$this->session->set_flashdata('message', $this->upload->display_errors());
		} else {
			$this->session->set_flashdata('message', 'Logo image has been regularly updated');


		}
	}

	public function uploadProducts()
	{
		$dir = 'assets/companies/' . $this->data['company']->directory . '/products';
		$config['upload_path'] = $dir;
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 2048;
		$this->load->library('upload', $config);

		if (!is_dir($dir)) {
			mkdir($dir);
		}

		if ($this->upload->do_upload('product_image')) {
			$uploadedImage = $this->upload->data();
			if ($this->resizeImage($uploadedImage['file_name'], $dir)) {
				return $uploadedImage['file_name'];
			}
			return '';
		}
	}

	public function resizeImage($filename, $dir)
	{
		$source_path = $_SERVER['DOCUMENT_ROOT'] . '/' . $dir . '/' . $filename;
		$target_path = $_SERVER['DOCUMENT_ROOT'] . '/' . $dir . '/thumbnail/';

		if (!is_dir($dir . '/thumbnail/')) {
			mkdir($dir . '/thumbnail/');
		}

		$config_manip = array(
			'image_library' => 'gd2',
			'source_image' => $source_path,
			'new_image' => $target_path,
			'maintain_ratio' => TRUE,
			'create_thumb' => TRUE,
			'thumb_marker' => '_thumb',
			'width' => 150,
			'height' => 150
		);

		$this->load->library('image_lib', $config_manip);
		if (!$this->image_lib->resize()) {
			return false;
		}
		$this->image_lib->clear();
		return true;
	}


}

<?php


class Login extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
//		$this->load->library('encrypt');
		$this->load->helper('url');
		$this->load->model('usermodel');


	}


	public function index()
	{
		$this->page = 'login';
		$this->layout();

	}

	public function validation(){
		$this->form_validation->set_rules('user_email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('user_password', 'Password', 'required');

		if ($this->form_validation->run()) {
			//$encryptedPassword = $this->encrypt->encode($this->input->post('user_password'));
			$encryptedPassword = sha1($this->input->post('user_password'));

			$result = $this->usermodel->canLogin($this->input->post('user_email'), $encryptedPassword);
			if($result === true){
				redirect('dashboard');
			}else{
				$this->session->set_flashdata('message', $result);
				redirect('login');
			}

		}else{
			$this->index();
		}
	}
}

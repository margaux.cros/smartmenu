<?php


class Register extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
//		$this->load->library('encrypt');
		$this->load->model('usermodel');
		$this->load->helper('url');

	}

	public function index()
	{
		$this->page = 'register';
		$this->layout();
	}

	public function validation()
	{

		$this->form_validation->set_rules('user_email', 'Email Adress', 'required|trim|valid_email|is_unique[user.email]');
		$this->form_validation->set_rules('user_name', 'Name', 'required');
		$this->form_validation->set_rules('user_password', 'Password', 'required');

		if ($this->form_validation->run()) {
			$verificationKey = md5(rand());
//			$encryptedPassword = $this->encrypt->encode($this->input->post('user_password'));
			$encryptedPassword = sha1($this->input->post('user_password'));
			$registerData = array(
				'email' => $this->input->post('user_email'),
				'name' => $this->input->post('user_name'),
				'password' => $encryptedPassword,
				'verification_key' => $verificationKey
			);
			$id = $this->usermodel->insert($registerData);
			if ($id > 0) {
				$subject = "Please verify email for login";
				$message = "
				<h1>" . $this->input->post('user_name') . "</h1>
				<p>This is email verification from Smartemenu Register system to complete registration process and login into system. First you need to verify your email adress by clicking <a href='" . base_url() . "register/verifyEmail/$verificationKey'>the link</a>.</p>
				<p>Once you click this link, your email adress will be verified and you can login the system</p>
				<p>Thanks,</p>
				";
				$config = array(
					'protocol' => 'smtp',
					'smtp_host' => 'smtpout.secureserver.net',
					'smtp_port' => 587,
					'smtp_user' => '',
					'smtp_pass' => '',
					'mailtype' => 'html',
					'charset' => 'utf-8',
					'wordwrap' => TRUE
				);


				$this->load->library('email', $config);
				$this->email->set_newline("\r\n");
				$this->email->from('support@smartmenu.com');
				$this->email->to($this->input->post('user_email'));
				$this->email->subject($subject);
				$this->email->message($message);
				if ($this->email->send()) {
					$this->session->set_flashdata('message', 'A confirmation email has been sent to ' . $this->input->post('user_email') . '. Check in your email for email verification mail');
					redirect('register');
				}

			}
		} else {
			$this->index();
		}
	}

	public function verifyEmail()
	{
		if ($this->uri->segment(3)) {
			$verificationKey = $this->uri->segment(3);
			if ($this->usermodel->verifyEmail($verificationKey)) {

				$data['message'] = '<div class="alert alert-success p-3">Congratulations ! Your email has been successfully verified. You can now login from <a href="' . base_url() . 'login">here</a></div>';
			} else {
				$data['message'] = '<div class="alert alert-danger p-3">Invalid link</div>';
			}

			$this->page = 'email_verification';
			$this->data = $data;
			$this->layout();
		}
	}
}

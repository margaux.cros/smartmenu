<?php


class Home extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');

	}




	public function index()
	{

		if($this->session->has_userdata('id')>0){
			redirect('dashboard');
		}

		$this->page = 'home';
		$this->layout();
		//$this->load->view('home');
	}

	public function logout(){
		$userSession = array('id');
		$this->session->unset_userdata($userSession);

		$this->index();
	}
}
